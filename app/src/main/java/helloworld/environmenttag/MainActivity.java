package helloworld.environmenttag;

import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

public class MainActivity extends AppCompatActivity {
    private BottomBar mBottomBar;
    private TextView t;
    private MainFragment mainFragment;
    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        instance = this;

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                int stackHeight = getSupportFragmentManager().getBackStackEntryCount();
                if (stackHeight > 0) { // if we have something on the stack (doesn't include the current shown fragment)
                    getSupportActionBar().setHomeButtonEnabled(true);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    getSupportActionBar().setHomeButtonEnabled(false);
                }
            }

        });

        mBottomBar = BottomBar.attach(this, savedInstanceState);
        mBottomBar.useFixedMode();
        mBottomBar.setItemsFromMenu(R.menu.bottombar_menu, new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(@IdRes int menuItemId) {
                // Clear back stack when user click bottom bar
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                if (menuItemId == R.id.home) {
                    Log.d("TAG", "Main Fragment");
                    MainFragment newFragment = new MainFragment();
                    mainFragment = newFragment;
                    Bundle args = new Bundle();
                    newFragment.setArguments(args);

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    // Replace fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.fragment_container, newFragment);

                    // Commit the transaction
                    transaction.commit();
                } else if (menuItemId == R.id.history) {
                    Log.d("TAG", "History Fragment");
                    HistoryFragment newFragment = new HistoryFragment();
                    Bundle args = new Bundle();
                    newFragment.setArguments(args);

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.fragment_container, newFragment);

                    // Commit the transaction
                    transaction.commit();
                } else if (menuItemId == R.id.settings) {
                    Log.d("TAG", "Settings Fragment");
                    SettingFragment newFragment = new SettingFragment();
                    Bundle args = new Bundle();
                    newFragment.setArguments(args);

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    // Replace whatever is in the fragment_container view with this fragment,
                    // and add the transaction to the back stack so the user can navigate back
                    transaction.replace(R.id.fragment_container, newFragment);

                    // Commit the transaction
                    transaction.commit();
                }
            }

            @Override
            public void onMenuTabReSelected(@IdRes int menuItemId) {
                if (menuItemId == R.id.home) {
                    // The user reselected item number one, scroll your content to top.
                    mainFragment.hideModal();
                }
            }
        });
    }

    public void onSelectNoOfRec(View view) {
        mainFragment.onSelectNoOfRec(view);
    }

    public void onSelectAdd(View view) {
        mainFragment.onSelectAdd(view);
    }

    public void onSelectReadCurrentTemp(View view) {
        mainFragment.onSelectReadCurrentTemp(view);
    }

    public void onSelectChart(View view) {
        mainFragment.onSelectChart(view);
    }

    /* Function to generate read temperature command from particular offset */
    public byte[] readTempCmdFromOffset(int offsetInt) {
        String digit = "";
        byte[] offsetByte = {};
        byte[] cmdHead = {0x24, 0x33, 0x32, 0x2C}; // $32,
        byte[] cmdTail = {0x2C, 0x2A, 0x32, 0x35, 0x0D, 0x0A}; // ,*25\r\n

        // Convert int offset to string digit
        // 1234 to "1234"
        String number = String.valueOf(offsetInt);
        for (int i = 0; i < number.length(); i++) {
            digit += Integer.toString(Character.digit(number.charAt(i), 10));
        }

        // Convert string digit to ASCII hex
        // e.g. "1234" to 0x31 0x32 0x33 0x34
        try {
            offsetByte = digit.getBytes("US-ASCII");
        } catch (Exception e) {
            t.append("Failed. " + e);
        }

        // Concatenate the CMD with offset, i.e. {cmdHead} {offsetByte} {cmdTail}
        byte[] cmd = new byte[cmdHead.length + offsetByte.length + cmdTail.length];
        System.arraycopy(cmdHead, 0, cmd, 0, cmdHead.length);
        System.arraycopy(offsetByte, 0, cmd, cmdHead.length, offsetByte.length);
        System.arraycopy(cmdTail, 0, cmd, cmdHead.length + offsetByte.length, cmdTail.length);

        return cmd;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Necessary to restore the BottomBar's state, otherwise we would
        // lose the current tab on orientation change.
        mBottomBar.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getSupportFragmentManager().popBackStack();
        return true;
    }

    @Override
    public void onPause() {
        EnvoTag_HID.getInstance(getBaseContext()).removeEvent();
        super.onPause();
    }

    @Override
    public void onResume(){
        EnvoTag_HID.getInstance(getBaseContext()).initEvent();
        super.onResume();
    }

}
