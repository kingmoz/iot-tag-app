package helloworld.environmenttag;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.AsyncTask;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import helloworld.environmenttag.HIDHelper.HIDEventListener;

/**
 * Created by ivan on 14/6/2016.
 */
public class EnvoTag_HID extends HID{
    private static final int SLEEP_TIME = 100;
    private static final int RETRY_TIME = 5;
    private static final int MISC_POS = 2093056;
    private AsyncTask _mainthread = null;
    private HID hid;
    private static EnvoTag_HID instance;
    private HashMap<String, Object> configObject;
    private HashMap<String,ArrayList<HIDEventListener>> listeners = new HashMap<String,ArrayList<HIDEventListener>>() {{
        put("attach",new ArrayList<HIDEventListener>());
        put("detach",new ArrayList<HIDEventListener>());
    }};

    public EnvoTag_HID(Context context){
        super(context,1638,1155);
        super.initEvent();
        hid = instance = this;
        if(super.OpenDevice()){
            hid.StartReadingThread();
            getMiscInfo();
        }
    }

    public static EnvoTag_HID getInstance(Context context){
        if(instance == null) {
            instance = new EnvoTag_HID(context);
        }
        return instance;
    }

    /**
     * @return successful construct or not
     */
    public boolean isRunning(){
        return super.ReadingThreadStatus();
    }

    @Override
    protected void onDetached(){
        super.onDetached();
        clearMiscInfo();
        for(HIDEventListener hlis : listeners.get("detach")){
            hlis.onDeviceDetached();
        }
    }

    @Override
    protected void onNewDevice(UsbDevice device){
        super.onNewDevice(device);
        getMiscInfo();
        for(HIDEventListener hlis : listeners.get("attach")){
            hlis.onDeviceFound();
        }
    }

    @Override
    public void initEvent(){
        super.initEvent();
    }

    @Override
    public void removeEvent(){
        super.removeEvent();
    }

    //=============+++  Listener function +++================//

    /**
     * Add event Listener for callback
     * @param event name of the event(detach,attach)
     * @param callback interface that have the callback
     */
    public void addEventListener(String event,HIDEventListener callback){
        if(event !="attach" && event != "detach")
            return;
        ArrayList<HIDEventListener> list = listeners.get(event);
        list.add(callback);
    }

    /**
     * Remove explicit listener for specific event
     * @param event name of the event(detach,attach)
     * @param callback interface register in callback
     */
    public void removeEventListener(String event,HIDEventListener callback){
        if(event !="attach" && event != "detach")
            return;
        ArrayList<HIDEventListener> list = listeners.get(event);
        list.remove(callback);
    }

    /**
     * Clear all Listener for specific event
     * @param event name of the event
     */
    public void clearEventListenerr(String event){
        if(event !="attach" && event != "detach")
            return;
        (listeners.get(event)).clear();
    }

    /**
     * Clear all registered event
     */
    public void clearEventListener(){
        (listeners.get("attach")).clear();
        (listeners.get("detach")).clear();
    }

    //======================+++  getter and setter of HID  +++==========================//




    /* Function to check no. of temperature record against temperature stored in tempArray[] */
    protected boolean checkNumOfRec(float tempArray[], int numOfRec){
        if(tempArray.length == numOfRec)
            return true;
        else
            return false;
    }

    /* Function to send reset command to EnvoTag
     * return true if reset success */
    protected boolean resetTag(){
        byte[] cmd = {0x24, 0x31, 0x34, 0x2C, 0x34, 0x2C, 0x2A, 0x32, 0x41, 0x0D, 0x0A};    //$14,475,*2A\r\n

        try {
            readWriteFlow(cmd);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    /** Function to return total no. of temperature record
     * @return int number of record -999 if failed
     */
    protected int getNumOfRec() {
        int numOfRec = -999;
        byte[] result;
        byte[] cmd = {0x24, 0x32, 0x34, 0x2C, 0x2A, 0x32, 0x41, 0x0D, 0x0A};// $24,*2A\r\n

        try {
            result = readWriteFlow(cmd);
            numOfRec = bytesToTotalNumOfRec(result);
        }catch(Exception e){
            //do nothing
        }
        return numOfRec;
    }

    /* Function to return current timestamp
     * return -999 if failed */
    protected int getCurrentTime() {
        int timestamp = -999;
        byte[] result;
        byte[] cmd = {0x24, 0x31, 0x33, 0x2C, 0x2A, 0x32, 0x45, 0x0D, 0x0A};    // $13,*2E\r\n
        try {
            result = readWriteFlow(cmd);
            timestamp = Integer.parseInt((bytesToHex(Arrays.copyOfRange(result, 0, 4))), 16);
        }catch(Exception e){
            //do nothing
        }
        return timestamp;
    }

    /* Function to return record start timestamp
     * return -999 if failed */
    protected int getRecordStartTime() {
        byte[] result;
        byte[] cmd = {0x24, 0x32, 0x33, 0x2C, 0x2A, 0x32, 0x45, 0x0D, 0x0A};    // $23,*2E\r\n
        int timestamp = -999;

        try {
            result = readWriteFlow(cmd);
            timestamp = Integer.parseInt((bytesToHex(Arrays.copyOfRange(result, 0, 4))), 16);
        }catch(Exception e){
            //do nothing
        }
        return timestamp;
    }

    /* Function to read firmware version from EnvoTag */
    protected String readFirmwareVersion(){
        byte[] result;
        byte[] cmd = {0x24, 0x30, 0x39, 0x2C, 0x2A, 0x32, 0x35, 0x0D, 0x0A}; // $09,*25\r\n
        String firmware = "";

        try {
            result = readWriteFlow(cmd);
            firmware = bytesToHex(Arrays.copyOfRange(result, 0, 4));
        }catch(Exception e){
            //do nothing
        }
        return firmware;
    }

    /* Function to read serial number from EnvoTag */
    protected int readSerialNum(){
        byte[] result;
        byte[] cmd = {0x24, 0x31, 0x31, 0x2C, 0x2A, 0x32, 0x43, 0x0D, 0x0A}; // $11,*2C\r\n
        int serialNum = -999;

        try {
            result = readWriteFlow(cmd);
            serialNum = Integer.parseInt(bytesToHex(Arrays.copyOfRange(result, 0, 4)), 16);
        }catch(Exception e){
            //do nothing
        }
        return serialNum;
    }

    /* Set serial number
     * return true if success */
    protected boolean setSerialNumber(int serialNum){
        byte[] cmd = getSerialNumCmd(serialNum);
        try {
            readWriteFlow(cmd);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    /* Set record interval
     * return true if success */
    protected boolean setRecordInterval(int interval){
        byte[] cmd = getRecordIntervalCmd(interval);
        try {
            readWriteFlow(cmd);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    /* Get record interval */
    protected int getRecordInterval(){
        byte[] result;
        byte[] cmd = {0x24, 0x32, 0x31, 0x2C, 0x2A, 0x32, 0x46, 0x0D, 0x0A}; // $21,*2F\r\n;
        int interval = -999;

        try {
            result = readWriteFlow(cmd);
            interval = Integer.parseInt(bytesToHex(Arrays.copyOfRange(result, 0, 1)), 16);
        }catch(Exception e){
            //do nothing
        }
        return interval;
    }

    /* Set high temperature limit
     * return true if success */
    protected boolean setHiTempLimit(float temp){
        byte[] cmd = getHiTempLimitCmd(temp);// generate set high temperature limit cmd
        try {
            readWriteFlow(cmd);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    /* Get high temperature limit */
    protected float getHiTempLimit(){
        byte[] result;
        byte[] cmd = {0x24, 0x33, 0x36, 0x2C, 0x2A, 0x32, 0x43, 0x0D, 0x0A}; // $36,*2C\r\n;
        float hiTemp = -999;

        try {
            result = readWriteFlow(cmd);
            hiTemp = bytesToTemp(result);
        }catch(Exception e){
            //do nothing
        }
        return hiTemp;
    }

    /* Set low temperature limit
     * return true if success */
    protected boolean setLoTempLimit(float temp){
        byte[] cmd = getLoTempLimitCmd(temp);
        try {
            readWriteFlow(cmd);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    /* Get low temperature limit */
    protected float getLoTempLimit(){
        byte[] result;
        byte[] cmd = {0x24, 0x33, 0x37, 0x2C, 0x2A, 0x32, 0x43, 0x0D, 0x0A}; // $37,*2C\r\n;
        float loTemp = -999;

        try {
            result = readWriteFlow(cmd);
            loTemp = bytesToTemp(result);
        }catch(Exception e){
            //do nothing
        }
        return loTemp;
    }

    /* Send CMD to EnvoTag to read current temp
       return -999 if current temp is unavailable */
    protected float readCurrentTemp(){
        byte[] result;
        byte[] cmd = {0x24, 0x31, 0x32, 0x2C, 0x30, 0x2C, 0x2A, 0x33, 0x33, 0x0D, 0x0A}; // $12,0,*33\r\n
        float currentTemp = -999;
        try {
            result = readWriteFlow(cmd);
            currentTemp = bytesToTemp(Arrays.copyOfRange(result, 0, 2));
        }catch(Exception e){
            //do nothing
        }
        return currentTemp;
    }

    /* Function to return temperature record of a particular page (index) */
    private float[] readOnePageTemp(int index){
        // calculate cmd page offset, e.g. 0, 64, 256, ...
        byte[] cmd = getReadTempCmdFromOffset(index * 64);
        List<Float> tempList = new ArrayList<Float>();
        float[] tempArray = {};
        try {
            byte[] result = readWriteFlow(cmd);
            if(result != null) {
                for (int i = 0; i < result.length && i < 64 && result[i] != (byte) 0xFF; i += 2) {// quit if the 32th record is retrieved OR encounter empty record (0xFF)
                    byte[] buf = {result[i], result[i + 1]};    // each temperature cost 2 bytes
                    tempList.add(bytesToTemp(buf));
//                float temp = bytesToTemp(buf);
//                tempArray = addElement(tempArray, temp);
                }
            }
            tempArray = objectToPrimitive(tempList);

        }catch(Exception e){
            //do nothing
        }
        return tempArray;
    }

    /* Function to read all temperature record from EnvoTag
     * return float array */
    protected float[] readAllTemp(){
        float[] tempArray = {};
        int numOfPage = getNumOfPage(getNumOfRec());

        for(int index = 0; index < numOfPage; index++){
            tempArray = concat(tempArray, readOnePageTemp(index));
        }

        return tempArray;
    }

    /* Function to read misc info from EnvoTag
     *      - START_TIME = record start time
     *      - RECORD_NUM = total no. of temp record
     *      - OUT_RANGE_NUM = no. of record out of range
     *      - INTERVAL = record interval
     *      - SERIAL_NUM = serial no.
     *      - HI_TEMP = hi temp threshold
     *      - LO_TEMP = lo temp threshold
     * return hash map */
    protected HashMap getMiscInfo(){
        if (configObject == null) {
            byte[] result = {};
            byte[] cmd = getReadTempCmdFromOffset(MISC_POS);
            configObject = new HashMap<String, Object>();

            try {
                result = readWriteFlow(cmd);

                if (result == null) {
                    configObject = null;
                } else {
                    configObject.put("START_TIME", Integer.parseInt(bytesToHex(Arrays.copyOfRange(result, 4, 8)), 16));
                    configObject.put("RECORD_NUM", Integer.parseInt(bytesToHex(Arrays.copyOfRange(result, 12, 16)), 16) / 2);
                    configObject.put("OUT_RANGE_NUM", Integer.parseInt(bytesToHex(Arrays.copyOfRange(result, 16, 17)), 16));
                    configObject.put("INTERVAL", Integer.parseInt(bytesToHex(Arrays.copyOfRange(result, 17, 18)), 16));
                    configObject.put("SERIAL_NUM", Integer.parseInt(bytesToHex(Arrays.copyOfRange(result, 18, 22)), 16));
                    configObject.put("HI_TEMP", bytesToTemp(Arrays.copyOfRange(result, 23, 25)));
                    configObject.put("LO_TEMP", bytesToTemp(Arrays.copyOfRange(result, 25, 27)));
                }
            } catch(NullPointerException e){
                //do nothing
            }
        }
        return configObject;
    }

    public void clearMiscInfo() {
        configObject = null;
    }


    /****************************************/
    /************ Helper Function ***********/
    /****************************************/
//    /* Function to start HID reading thread */
//    private class thread extends AsyncTask<String, Void, String> {
//        @Override
//        protected String doInBackground(String... string) {
//            hid.StartReadingThread();
//            return "";
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//        }
//    }

    /* Function to sleep a thread */
    private void Sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /* Function to merge 2 float array  */
    private float[] concat(float[] a, float[] b) {
        int aLen = a.length;
        int bLen = b.length;
        float[] c= new float[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    /* Function to convert 4 bytes to total no. of temperature record */
    private int bytesToTotalNumOfRec(byte[] bytes) {
        String str = bytesToHex (bytes);
        return Integer.parseInt(String.valueOf(Long.parseLong(str.substring(0, 8), 16) / 2));
    }

    /* Function to convert byte array to hex array (string) */
    private static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];

        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    /* Function to convert 2 bytes to temperature */
    private float bytesToTemp(byte[] bytes) {
        return (float)((ByteBuffer.wrap(bytes).getShort() - 400) / 10.0);
    }

    /* Function to calculate total no. of page */
    private int getNumOfPage(int numOfTemp) {
        return roundUp(numOfTemp, 32);
    }

    /* Function to do round-up division */
    private static int roundUp(int num, int divisor) {
        return (num + divisor - 1) / divisor;
    }

//    /* Function to add element e to the end of array a */
//    private static float[] addElement(float[] a, float e) {
//        a = Arrays.copyOf(a, a.length + 1);
//        a[a.length - 1] = e;
//        return a;
//    }

    private float[] objectToPrimitive(List<Float> list){
        float[] temp = new float[list.size()];
        for(int i = 0 ; i < list.size() ; i++){
            Float f = list.get(i);
            temp[i] = (f != null ? f : Float.NaN);
        }
        return temp;
    }

    private byte[] readWriteFlow(byte[] cmd) throws NullPointerException{
        int tryCount = 0;
        boolean sendSuccess = false;
        byte[] result = null;

        while (tryCount < RETRY_TIME) {
            try {
                sendSuccess = hid.WriteData(cmd);
            } catch (Exception e) {
                sendSuccess = false;
            }

            if(sendSuccess){
                try {
                    if (hid.IsThereAnyReceivedData()) {
                        result = hid.GetReceivedDataFromQueue();
                        break;
                    }
                } catch (Exception e) {
                    break;
                }
            }
            Sleep(SLEEP_TIME);
            tryCount++;
        }

        if(!sendSuccess) throw  new NullPointerException();

        return result;
    }

    private byte[] generalSetCmd(byte[] cmdHead,byte[] cmdTail,String value){
        String digit = "";
        byte[] offsetByte = {};

        // Convert int offset to string digit
        // 1234 to "1234"
        String number = value;
        for (int i = 0; i < number.length(); i++) {
            digit += Integer.toString(Character.digit(number.charAt(i), 10));
        }

        // Convert string digit to ASCII hex
        // e.g. "1234" to 0x31 0x32 0x33 0x34
        try {
            offsetByte = digit.getBytes("US-ASCII");
        } catch (Exception e) {
            // do nothing
        }

        // Concatenate the CMD with offset, i.e. {cmdHead} {offsetByte} {cmdTail}
        byte[] cmd = new byte[cmdHead.length + offsetByte.length + cmdTail.length];
        System.arraycopy(cmdHead, 0, cmd, 0, cmdHead.length);
        System.arraycopy(offsetByte, 0, cmd, cmdHead.length, offsetByte.length);
        System.arraycopy(cmdTail, 0, cmd, cmdHead.length + offsetByte.length, cmdTail.length);

        return cmd;
    }

    /* Function to generate set serial number command */
    private byte[] getSerialNumCmd(int serialNum) {
        byte[] cmdHead = {0x24, 0x31, 0x30, 0x2C}; // $10,
        byte[] cmdTail = {0x2C, 0x2A, 0x31, 0x35, 0x0D, 0x0A}; // ,*15\r\n
        return generalSetCmd(cmdHead,cmdTail,String.valueOf(serialNum));
    }

    /* Function to generate record interval setting command from particular interval */
    private byte[] getRecordIntervalCmd(int interval) {
        byte[] cmdHead = {0x24, 0x32, 0x30, 0x2C}; // $20,
        byte[] cmdTail = {0x2C, 0x2A, 0x31, 0x35, 0x0D, 0x0A}; // ,*15\r\n
        return generalSetCmd(cmdHead,cmdTail,String.valueOf(interval));
    }

    /* Function to generate high temperature limit setting command */
    private byte[] getHiTempLimitCmd(float temp) {
        byte[] cmdHead = {0x24, 0x33, 0x34, 0x2C}; // $34,
        byte[] cmdTail = {0x2C, 0x2A, 0x31, 0x35, 0x0D, 0x0A}; // ,*15\r\n
        return generalSetCmd(cmdHead,cmdTail,String.valueOf(temp));
    }

    /* Function to generate high temperature limit setting command */
    private byte[] getLoTempLimitCmd(float temp) {
        byte[] cmdHead = {0x24, 0x33, 0x35, 0x2C}; // $35,
        byte[] cmdTail = {0x2C, 0x2A, 0x31, 0x35, 0x0D, 0x0A}; // ,*15\r\n
        return generalSetCmd(cmdHead,cmdTail,String.valueOf(temp));
    }

    /* Function to generate read temperature command from particular offset */
    private byte[] getReadTempCmdFromOffset(int offsetInt) {
        byte[] cmdHead = {0x24, 0x33, 0x32, 0x2C}; // $32,
        byte[] cmdTail = {0x2C, 0x2A, 0x32, 0x35, 0x0D, 0x0A}; // ,*25\r\n
        return generalSetCmd(cmdHead,cmdTail,String.valueOf(offsetInt));
    }


}
