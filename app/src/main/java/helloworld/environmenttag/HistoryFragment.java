package helloworld.environmenttag;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by king on 5/6/2016.
 */
public class HistoryFragment extends Fragment {
    private final static String TAG = "history";
    private List<String> listHeader = new ArrayList<>();
    HashMap<String, List<HistoryListChild>> listDataChild = new HashMap<>();
    private ExpandableListAdapter expandAdapter;
    private ExpandableListView expandListView;
    boolean mAlreadyLoaded = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.content_history, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Update Action Bar
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle("History");

        if (savedInstanceState == null && !mAlreadyLoaded) {
            mAlreadyLoaded = true;

            // Register click event
            updateExpandListView();
            updateHistoryList();
        } else {
            updateExpandListView();
            expandListView.setAdapter(expandAdapter);
            getView().findViewById(R.id.historyModal).setVisibility(View.INVISIBLE);
        }
    }

    private void updateExpandListView() {
        expandListView = (ExpandableListView) getView().findViewById(R.id.elist);
        expandListView.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                HistoryListChild currentChild = listDataChild.get(listHeader.get(groupPosition)).get(childPosition);
                Log.d(TAG, "groupPosition: " + groupPosition + " childPosition: " + childPosition + " currentChild ID: " + currentChild.getID());
                DetailsFragment newFragment = new DetailsFragment();
                Bundle args = new Bundle();
                //HistoryListChild currentChild= (HistoryListChild) listDataChild.get(listHeader.get(groupPosition));
                args.putLong("ID", currentChild.getID());
                newFragment.setArguments(args);

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack so the user can navigate back
                transaction.replace(R.id.fragment_container, newFragment);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
                return false;
            }
        });
    }


    private void updateHistoryList() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat headerDateFormat = new SimpleDateFormat("yyyy/MM/dd");

        // Get ListView obj from DB
        DBHelper dbHelper = new DBHelper(getContext());
        HashMap history = dbHelper.getHistoryModelList();
        String currentDay = "";
        List<HistoryListChild> newList = new ArrayList<>();
        Boolean firstRun = true;
        if (history != null && !history.isEmpty()) {
            Iterator it = history.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                HistoryModel historyModel = (HistoryModel) pair.getValue();
                if (currentDay.compareTo("" + headerDateFormat.format(historyModel.getStartTime())) != 0) {
                    if (!firstRun) {
                        Log.d(TAG, "currentDay : " + currentDay);
                        listHeader.add(currentDay);
                        listDataChild.put(currentDay, newList);
                        newList = new ArrayList<>();
                    } else {
                        firstRun = false;
                    }
                    currentDay = "" + headerDateFormat.format(historyModel.getStartTime());
                }
                newList.add(new HistoryListChild(
                        "" + dateFormat.format(historyModel.getStartTime()),
                        historyModel.getStatus(), Integer.parseInt(pair.getKey().toString())));
            }
            listHeader.add(currentDay);
            listDataChild.put(currentDay, newList);
            expandAdapter = new ExpandableListAdapter(getContext(),
                    listHeader,
                    listDataChild) {
            };
            expandListView.setAdapter(expandAdapter);
            getView().findViewById(R.id.historyModal).setVisibility(View.INVISIBLE);
        }
    }
}
