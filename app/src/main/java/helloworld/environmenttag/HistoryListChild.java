package helloworld.environmenttag;

/**
 * Created by king on 9/6/2016.
 */
public class HistoryListChild {
    private String Time;
    private int ID;
    private Boolean Status;

    HistoryListChild(String time, Boolean Status, int ID) {
        this.Time = time;
        this.Status = Status;
        this.ID = ID;
    }

    public String getTime() {
        return Time;
    }
    public void setName(String Time) {
        this.Time = Time;
    }
    public Boolean getStatus() {
        return Status;
    }
    public void setStatus(Boolean Status) {
        this.Status = Status;
    }
    public int getID() {
        return ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }
}
