package helloworld.environmenttag;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

/**
 * Created by king on 6/6/2016.
 */
public class SettingFragment extends Fragment {
    private EnvoTag_HID tag;
    private FragmentActivity parent;
    private LayoutInflater inflater;
    private double high_temperature = 0.0;
    private double low_temperature = 0.0;
    private int ui_interval[];
    private TextView high_temp_view;
    private TextView low_temp_view;
    private TextView time_interval_view;
    private TextView serial_num_view;
    private TextView version_num_view;
    private final String[] duration_units = {"MIN","HOUR"};
    private Bundle my_bundle;


    private int ui2interval(int interval,int duration_unit_index){
        return duration_unit_index == 0 ? interval : interval * 60;
    }

    private int[] interval2_ui(int interval){
        int[] temp = new int[2];
        if(interval / 60 < 1 ) {
            temp[0] = interval < 0 ? 0 : interval;
            temp[1] = 0;
        }else{
            temp[0] = interval /60;
            temp[1] = 1;
        }
        return temp;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.content_setting, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Update Action Bar
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("Settings");

        parent = this.getActivity();
        tag = EnvoTag_HID.getInstance(getContext());
//        tag.initTag(getContext());
        my_bundle = getArguments();

        //get view
        high_temp_view = (TextView) view.findViewById(R.id.high_temp);
        low_temp_view = (TextView) view.findViewById(R.id.low_temp);
        time_interval_view =  (TextView) view.findViewById(R.id.time_interval);
        serial_num_view =  (TextView) view.findViewById(R.id.serial_num);
        version_num_view =  (TextView) view.findViewById(R.id.version_num);

        if(savedInstanceState != null ) {
            setView(my_bundle.getString("version"),
                    my_bundle.getString("ser_no"),
                    my_bundle.getIntArray("ui_interval"),
                    my_bundle.getDouble("high_temperature"),
                    my_bundle.getDouble("low_temperature"));
        }

        //setview
        if(tag.isRunning()) {
            (new Thread() {
                public void run() {
                    syncData(my_bundle);
                }
            }).start();
        }

        inflater = getLayoutInflater(savedInstanceState);

        high_temp_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                on_temp_box_click("SET High Temperature",0);
            }
        });

        low_temp_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                on_temp_box_click("SET Low Temperature",1);
            }
        });

        time_interval_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                on_interval_click();
            }
        });
    }

    public void syncData(Bundle savedInstanceState){
        HashMap<String,Object>  hmap = tag.getMiscInfo();
        final String version =  tag.readFirmwareVersion();
        final int ser_no = Integer.valueOf(hmap.get("SERIAL_NUM").toString());
        Integer interval = Integer.valueOf(hmap.get("INTERVAL").toString());
        ui_interval = interval2_ui(interval.intValue());
//        high_temperature = (double) tag.getHiTempLimit();
        high_temperature = Double.valueOf(hmap.get("HI_TEMP").toString());
        low_temperature = Double.valueOf(hmap.get("LO_TEMP").toString());



//        savedInstanceState.putString("version",version);
//        savedInstanceState.putString("ser_no",String.valueOf(ser_no));
//        savedInstanceState.putIntArray("ui_interval",ui_interval);
//        savedInstanceState.putDouble("high_temperature",high_temperature);
//        savedInstanceState.putDouble("low_temperature",low_temperature);

        parent.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                Toast.makeText(parent.getBaseContext(), version, Toast.LENGTH_SHORT).show();
//                Toast.makeText(parent.getBaseContext(), ser_no, Toast.LENGTH_SHORT).show();
//                Toast.makeText(parent.getBaseContext(), ui_interval.toString(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(parent.getBaseContext(), high_temperature+"", Toast.LENGTH_SHORT).show();
//                Toast.makeText(parent.getBaseContext(), low_temperature+"", Toast.LENGTH_SHORT).show();
                setView(version,String.valueOf(ser_no),ui_interval,high_temperature,low_temperature);
            }
        });
    }

    public void setView(String version, String ser_no, int[] ui_interval,double high_temperature,double low_temperature){
        version_num_view.setText(version);
        serial_num_view.setText(String.valueOf(ser_no));
        if(ui_interval !=null) time_interval_view.setText(String.valueOf(ui_interval[0]) + " " + duration_units[ui_interval[1]]);
        high_temp_view.setText(high_temperature + "°C");
        low_temp_view.setText(low_temperature +  "°C");
    }

    public View create_temp_dialog_view(double temp){
        View alertLayout = inflater.inflate(R.layout.temp_dialog, null);

        final NumberPicker temp_sign = (NumberPicker) alertLayout.findViewById(R.id.temp_sign);
        final NumberPicker tempera = (NumberPicker) alertLayout.findViewById(R.id.temp);
        final NumberPicker temp_decimal = (NumberPicker) alertLayout.findViewById(R.id.temp_decimal);



        temp_sign.setMinValue(0);
        temp_sign.setMaxValue(1);
        temp_sign.setDisplayedValues( new String[] { "+", "-" } );
        if(temp < 0)
            temp_sign.setValue(1);
        else
            temp_sign.setValue(0);

        tempera.setMinValue(0);
        tempera.setMaxValue(60);
        tempera.setValue((int)Math.abs(temp));

        temp_decimal.setMinValue(0);
        temp_decimal.setMaxValue(9);
        temp_decimal.setValue((int)((temp-(int)temp)*10));

        return alertLayout;

    }

    public View create_interval_dialog_view(){
        View alertLayout = inflater.inflate(R.layout.time_interval, null);
        int ui_interval[] = interval2_ui(tag.getRecordInterval());

        final NumberPicker duration = (NumberPicker) alertLayout.findViewById(R.id.duration);
        final NumberPicker duration_unit = (NumberPicker) alertLayout.findViewById(R.id.duration_unit);

        duration_unit.setMinValue(0);
        duration_unit.setMaxValue(1);
        duration_unit.setDisplayedValues(duration_units);
        duration_unit.setValue(ui_interval[1]);

        duration.setMinValue(0);
        duration.setMaxValue(60);
        duration.setValue(ui_interval[0]);

        return alertLayout;
    }

    public void on_temp_box_click(String title,final int mode){

        View alertLayout = create_temp_dialog_view(mode == 0 ? high_temperature : low_temperature);

        final NumberPicker temp_sign = (NumberPicker) alertLayout.findViewById(R.id.temp_sign);
        final NumberPicker tempera = (NumberPicker) alertLayout.findViewById(R.id.temp);
        final NumberPicker temp_decimal = (NumberPicker) alertLayout.findViewById(R.id.temp_decimal);

        AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
        alert.setTitle(title);

        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);

        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(parent.getBaseContext(), "Canceled", Toast.LENGTH_SHORT).show();
            }
        });

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                double temperature = tempera.getValue() + temp_decimal.getValue() / 10;
                if(temp_sign.getValue() == 1) temperature = 0 - temperature;
                if(mode == 0){
                    high_temperature = temperature;
                    high_temp_view.setText(String.valueOf(temperature) + "°C");
                    tag.setHiTempLimit((float)high_temperature);
                }else{
                    low_temperature = temperature;
                    low_temp_view.setText(String.valueOf(temperature) + "°C");
                    tag.setLoTempLimit((float)low_temperature);
                }

                Toast.makeText(parent.getBaseContext(), "Temperature set : " + temperature, Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }



    public void on_interval_click(){

        View alertLayout = create_interval_dialog_view();

        final NumberPicker duration = (NumberPicker) alertLayout.findViewById(R.id.duration);
        final NumberPicker duration_unit = (NumberPicker) alertLayout.findViewById(R.id.duration_unit);

        AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
        alert.setTitle("Set Measure time interval");

        // this is set the view from XML inside AlertDialog
        alert.setView(alertLayout);

        // disallow cancel of AlertDialog on click of back button and outside touch
        alert.setCancelable(false);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(parent.getBaseContext(), "Canceled", Toast.LENGTH_SHORT).show();
            }
        });

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                time_interval_view.setText(duration.getValue() +" " + duration_units[duration_unit.getValue()] );
                tag.setRecordInterval(ui2interval(duration.getValue(),duration_unit.getValue()));
                Toast.makeText(parent.getBaseContext(), "Interval set : " + duration.getValue() + duration_units[duration_unit.getValue()] , Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

}
