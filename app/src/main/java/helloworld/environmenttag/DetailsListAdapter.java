package helloworld.environmenttag;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by king on 14/7/2016.
 */
public class DetailsListAdapter extends BaseAdapter {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_DETAILS = 1;
    private static final int TYPE_MAX_COUNT = 2;
    private ArrayList<DetailsFragment.detailsItem> mDetails;
    private DataStore.ConfigObject mHeader;
    private LayoutInflater mInflater;

    public DetailsListAdapter(Context context, DataStore.ConfigObject configObject, ArrayList<DetailsFragment.detailsItem> details) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mHeader = configObject;
        mDetails = details;
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_MAX_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? TYPE_HEADER : TYPE_DETAILS;
    }

    // Return total number of details + header
    @Override
    public int getCount() {
        return mDetails.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return position == 0 ? mHeader : mDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        int type = getItemViewType(position);
        if (convertView == null) {
            holder = new ViewHolder();
            switch (type) {
                case TYPE_HEADER:
                    convertView = mInflater.inflate(R.layout.details_list_header, null);
                    updateHeader(convertView);
                    break;
                case TYPE_DETAILS:
                    convertView = mInflater.inflate(R.layout.details_list_item, null);
                    holder.tempTextView = (TextView)convertView.findViewById(R.id.detailTemp);
                    holder.timeTextView = (TextView)convertView.findViewById(R.id.detailTime);
                    holder.tempTextView.setText(mDetails.get(position - 1).temperature);
                    holder.timeTextView.setText(mDetails.get(position - 1).time);
                    break;
            }
            convertView.setTag(holder);
        } else {
            if (type == TYPE_HEADER) {
                updateHeader(convertView);
            } else {
                holder = (ViewHolder)convertView.getTag();
                holder.tempTextView.setText(mDetails.get(position - 1).temperature);
                holder.timeTextView.setText(mDetails.get(position - 1).time);
            }
        }
        return convertView;
    }

    private void updateHeader(View convertView) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        TextView startTime = (TextView)convertView.findViewById(R.id.startTime);
        startTime.setText(df.format(mHeader.startTime));
        TextView endTime = (TextView)convertView.findViewById(R.id.readTime);

        endTime.setText(df.format(mHeader.startTime + mHeader.time_interval * mHeader.num_of_record * 1000));
        TextView timeInterval = (TextView)convertView.findViewById(R.id.timeInterval);
        timeInterval.setText(Integer.toString(mHeader.time_interval));
    }

    public static class ViewHolder {
        public TextView tempTextView;
        public TextView timeTextView;
    }

}
