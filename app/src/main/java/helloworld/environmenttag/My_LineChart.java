package helloworld.environmenttag;


import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import android.widget.SeekBar;



import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kwokyinlun on 5/11/15.
 */
public class  My_LineChart implements OnChartGestureListener, OnChartValueSelectedListener{

    private LineChart mChart;
    private SeekBar mSeekBarX, mSeekBarY;
    private LimitLine high_limit;
    private LimitLine low_limit;
    private EnvoTag_HID hid;

    public static enum TEMP_LIMIT{
        HIGH(1),LOW(0);
        private int value;
        TEMP_LIMIT(int val){
            this.value = val;
        }
    }

    public My_LineChart(){
//        hid = EnvoTag_HID.getInstance();
        System.out.println("view must be defined");
    }
    public My_LineChart(View view){
        mChart = (LineChart) view;
        mChart.setBorderColor(0);
        mChart.setDescription("");
        mChart.setNoDataTextDescription("You need to provide data for the chart.");
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setPinchZoom(true);
        mChart.setAutoScaleMinMaxEnabled(true);
        mChart.setBackgroundColor(Color.parseColor("#FFFFFF"));
        mChart.setGridBackgroundColor(Color.parseColor("#F8F8FF"));
        mChart.setAlpha(0.95f);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setDrawGridLines(false);
        xAxis.setAxisLineWidth(3f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis yAxis = mChart.getAxisRight();
        yAxis.setEnabled(false);
        yAxis.setDrawGridLines(false);
        YAxis yAxis2 = mChart.getAxisLeft();
        yAxis2.setDrawGridLines(false);
        yAxis2.setAxisLineWidth(3f);


        high_limit = new LimitLine(28f, "High Temp");
        high_limit.setLineWidth(1f);
        high_limit.setLineColor(Color.parseColor("#DC143C"));
//        ll1.enableDashedLine(10f, 10f, 0f);
        high_limit.setLabelPosition(LimitLabelPosition.RIGHT_TOP);
        high_limit.setTextSize(10f);

        low_limit = new LimitLine(-4f, "Low Temp");
        low_limit.setLineWidth(1f);
        low_limit.setLineColor(Color.parseColor("#1E90FF"));
//        ll2.enableDashedLine(10f, 10f, 0f);
        low_limit.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
        low_limit.setTextSize(10f);


        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.addLimitLine(high_limit);
        leftAxis.addLimitLine(low_limit);
//        leftAxis.setAxisMaxValue(40f);
//        leftAxis.setAxisMinValue(-20f);
        leftAxis.setStartAtZero(false);
        //leftAxis.setYOffset(20f);
//        leftAxis.enableGridDashedLine(10f, 10f, 0f);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

    }

    public void setLimitLine(float num,TEMP_LIMIT which){
        YAxis leftAxis = mChart.getAxisLeft();
        if(which == TEMP_LIMIT.HIGH){
            leftAxis.removeLimitLine(high_limit);
            high_limit = new LimitLine(num, "High Temp");
            high_limit.setLineWidth(1f);
            high_limit.setLineColor(Color.parseColor("#DC143C"));
            high_limit.setLabelPosition(LimitLabelPosition.RIGHT_TOP);
            high_limit.setTextSize(10f);
            leftAxis.addLimitLine(high_limit);
        }else{
            leftAxis.removeLimitLine(low_limit);
            low_limit = new LimitLine(num, "Low Temp");
            low_limit.setLineWidth(1f);
            low_limit.setLineColor(Color.parseColor("#1E90FF"));
            low_limit.setLabelPosition(LimitLabelPosition.RIGHT_BOTTOM);
            low_limit.setTextSize(10f);
            leftAxis.addLimitLine(low_limit);
        }
    }


    public void setData(ArrayList<String> x_labels, List<Float> tempeature){
        ArrayList<Entry> yVals = new ArrayList<Entry>();

        for (int i = 0; i < tempeature.size(); i++) {
            yVals.add(new Entry(tempeature.get(i).floatValue(), i));
        }

        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "");

        // set the line to be drawn like this "- - - - - -"
//        set1.enableDashedLine(10f, 5f, 0f);
//        set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.parseColor("#DDA0DD"));
        set1.setDrawCircles(false);
//        set1.setCircleColor(Color.BLACK);
//        set1.setLineWidth(3f);
//        set1.setCircleSize(2f);
//        set1.setDrawCircleHole(false);
        set1.setValueTextSize(0);
        set1.setFillAlpha(70);
        set1.setFillColor(Color.parseColor("#DDA0DD"));
        set1.setDrawFilled(true);
        set1.setDrawCubic(true);
        set1.getFillFormatter();
//        set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(), Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

        ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(x_labels, dataSets);
        // set data
        mChart.setData(data);
//        mChart.getAxisLeft().setStartAtZero(true);
//        mChart.getAxisRight().setStartAtZero(true);
        mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
        Legend l = mChart.getLegend();
        l.setForm(LegendForm.LINE);

    }


    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START");
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if(lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            mChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }


    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        Log.i("Entry selected", e.toString());
        Log.i("", "low: " + mChart.getLowestVisibleXIndex() + ", high: " + mChart.getHighestVisibleXIndex());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

}
