package helloworld.environmenttag.HIDHelper;

/**
 * Created by kwokyinlun on 25/6/16.
 */
public interface HIDEventListener {

    public void onDeviceFound();

    public void onDeviceDetached();
}
