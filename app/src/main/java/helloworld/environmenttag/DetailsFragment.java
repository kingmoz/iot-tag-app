package helloworld.environmenttag;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by king on 5/6/2016.
 */
public class DetailsFragment extends Fragment{
    ArrayList<detailsItem> listItems=new ArrayList<detailsItem>();
    ArrayAdapter<String> adapter;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.content_details, container, false);
}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Update Action Bar
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();

        DateFormat headerDateFormat = new SimpleDateFormat("yyyy/MM/dd  HH:mm:ss");

        // Get ListView object from xml
        listView = (ListView) getView().findViewById(android.R.id.list);

        long id = getArguments().getLong("ID", 0);
        DBHelper dbHelper = new DBHelper(getActivity().getApplicationContext());
        DataStore history = dbHelper.getHistory(id);
        ArrayList time = history.getTimeString_in_arraylist("yyyy/MM/dd HH:mm:ss");
        actionBar.setTitle(headerDateFormat.format(history.getInsertTime()));

        // Insert

        for (int i = 0; i < history.getTemperatures().size(); i++) {
            listItems.add(new detailsItem(history.getTemperatures().get(i) + "°C", "Record on " + time.get(i)));
        }

        DetailsListAdapter detailsListAdapter = new DetailsListAdapter(getContext(), history.getConfigObject(), listItems);
        listView.setAdapter(detailsListAdapter);

        ArrayList<String> xVals = history.getTimeString_in_arraylist("HH:mm:ss");
        My_LineChart mChart = new My_LineChart(getView().findViewById(R.id.detailChart));
        mChart.setData(xVals, history.getTemperatures());
    }

    public class detailsItem {
        public String temperature;
        public String time;
        public detailsItem(String _temperature, String _time) {
            temperature = _temperature;
            time = _time;
        }
    }
}
