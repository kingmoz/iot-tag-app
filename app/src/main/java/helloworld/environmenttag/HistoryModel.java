package helloworld.environmenttag;

/**
 * Created by King.Chan on 18/6/2016.
 */
public class HistoryModel {
    private Long startTime;
    private Long readTime;
    private boolean status;
    HistoryModel (Long startTime, Long readTime, int status) {
        this.startTime = startTime;
        this.readTime = readTime;
        this.status = status == 1;
    }
    public Long getStartTime() {
        return startTime;
    }
    public Long getReadTime() {
        return readTime;
    }
    public boolean getStatus() {
        return status;
    }
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }
    public void setReadTime(Long readTime) {
        this.readTime = readTime;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
}
