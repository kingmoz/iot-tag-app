package helloworld.environmenttag;

import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import java.net.HttpURLConnection;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;
import android.provider.Settings.Secure;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;



/**
 * Created by kwokyinlun on 20/2/16.
 */
public class HTTP_log{
    private static String android_id;


    public static void set_id(String id){
        android_id = id;
    }

    public static void set_id(Context context){
        android_id = Secure.getString(context.getContentResolver(),Secure.ANDROID_ID);
    }


    public static void post_msg(String msg){
        new HTTP_POST().execute(msg);
    }

    private static class HTTP_POST extends AsyncTask<String, Void,Boolean>{
        private HttpURLConnection urlConnection;
        protected Boolean doInBackground(String... params) {
            try {

                URL url = new URL("http://envotag-cuhelloworld.rhcloud.com/");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");



                JSONObject jsonParam = new JSONObject();
                jsonParam.put("ID", android_id);
                jsonParam.put("message", params[0]);


                OutputStream out = urlConnection.getOutputStream();
                out.write(jsonParam.toString().getBytes("UTF-8"));
                out.flush();
    //            out.close();

    //            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
    //                throw new RuntimeException("Failed : HTTP error code : "
    //                        + urlConnection.getResponseCode());
    //            }

                InputStream in = urlConnection.getInputStream();
                String res = readStream(in);
                Log.d("respond:",res);
                return true;


            } catch (MalformedURLException e) {
                Log.d("url","wrong url");
            }catch (IOException e) {
                e.printStackTrace();
                Log.d("io","fail input or output");
            }catch(JSONException e){
                e.printStackTrace();
            }catch(RuntimeException e){
                e.printStackTrace();
            }finally {
                if (urlConnection != null) {
                    Log.d("URLconecct","close");
                    urlConnection.disconnect();}
            }

            return false;
        }

        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }

    }

}
