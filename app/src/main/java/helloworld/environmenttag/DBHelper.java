package helloworld.environmenttag;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    // TODO: move history as a class
    public static final String DATABASE_NAME = "envotag.db";
    public static final String TABLE_NAME = "history";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_START_TIME = "start_time";
    public static final String COLUMN_READ_TIME = "read_time";
    public static final String COLUMN_INTERVAL = "interval";
    public static final String COLUMN_TEMP = "temp";
    public static final String COLUMN_SERIAL_NO = "serial_no";
    public static final String COLUMN_INSERT_TIME = "insert_time";
    public static final String COLUMN_UPPER_LIMIT = "upper_limit";
    public static final String COLUMN_LOWER_LIMIT = "lower_limit";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_NO_OF_RECORD = "no_of_record";

    public static final String CREATE_SQL =
            "CREATE TABLE " + TABLE_NAME +"("+
                    COLUMN_ID + " integer primary key autoincrement, "+
                    COLUMN_START_TIME + " integer, "+
                    COLUMN_READ_TIME + " integer,"+
                    COLUMN_INTERVAL + " integer, "+
                    COLUMN_TEMP + " text, "+
                    COLUMN_SERIAL_NO + " text, "+
                    COLUMN_INSERT_TIME + " integer, "+
                    COLUMN_UPPER_LIMIT + " integer, "+
                    COLUMN_LOWER_LIMIT + " integer, "+
                    COLUMN_STATUS + " integer, " +
                    COLUMN_NO_OF_RECORD + " integer);";

    public static final String unique_SQL = "Create UNIQUE INDEX se_start on " + TABLE_NAME +"("+COLUMN_START_TIME+","+COLUMN_SERIAL_NO+")";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(CREATE_SQL);
        db.execSQL(unique_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void onTruncate(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public long insertHistory(long start_time, int interval, String temp,String ser_no,long insert_time,float upper,float lower, int no_of_record){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_START_TIME, start_time);//1
        contentValues.put(COLUMN_READ_TIME, new Date().getTime());
        contentValues.put(COLUMN_INTERVAL, interval);//2
        contentValues.put(COLUMN_TEMP, temp);//3
        contentValues.put(COLUMN_SERIAL_NO, ser_no);//4
        contentValues.put(COLUMN_INSERT_TIME, insert_time);//5
        contentValues.put(COLUMN_UPPER_LIMIT, upper);//6
        contentValues.put(COLUMN_LOWER_LIMIT, lower);//7
        contentValues.put(COLUMN_STATUS, 1);//fuck
        contentValues.put(COLUMN_NO_OF_RECORD, no_of_record);
        return db.insert(TABLE_NAME, null, contentValues);
    }

    public DataStore getHistory(long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("get_his debug",""+id);
        Cursor res = db.query(TABLE_NAME, null, COLUMN_ID + " = " + id,null, null, null, null,null);
//        Cursor res = db.rawQuery("Select * from history where id = ?",new String[]{Integer.toString(id)});
//        Log.d("history has data",""+res.moveToFirst());
        if(res.moveToFirst()) {
            String ser_no = res.getString(res.getColumnIndex(COLUMN_SERIAL_NO));
            int interval = res.getInt(res.getColumnIndex(COLUMN_INTERVAL));
            int upper = res.getInt(res.getColumnIndex(COLUMN_UPPER_LIMIT));
            int lower = res.getInt(res.getColumnIndex(COLUMN_LOWER_LIMIT));
            long sta_time = res.getLong(res.getColumnIndex(COLUMN_START_TIME));
            String temp = res.getString(res.getColumnIndex(COLUMN_TEMP));
            long insert_ti = res.getLong(res.getColumnIndex(COLUMN_INSERT_TIME));
            int no_of_record = res.getInt(res.getColumnIndex(COLUMN_NO_OF_RECORD));
            DataStore hi = new DataStore(ser_no, interval, upper, lower, sta_time, temp, insert_ti, no_of_record);
            res.close();
            return hi;
        }else{
            res.close();
            return null;
        }
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

    public boolean updateHistory(int id,long start_time, int interval, String temp,String ser_no,long insert_time,float upper,float lower){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_START_TIME, start_time);
        contentValues.put(COLUMN_INTERVAL, interval);
        contentValues.put(COLUMN_TEMP, temp);
        contentValues.put(COLUMN_SERIAL_NO, ser_no);
        contentValues.put(COLUMN_INSERT_TIME, insert_time);
        contentValues.put(COLUMN_UPPER_LIMIT, upper);
        contentValues.put(COLUMN_LOWER_LIMIT, lower);
        db.update(TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteHistory(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME,"id = ? ",new String[] { Integer.toString(id) });
    }

    private String getRecord(Cursor cus){
        return null;
    }

    public HashMap getAllHistoryName(){
        HashMap hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.query(TABLE_NAME, new String[]{COLUMN_ID, COLUMN_START_TIME},null,null,null,null,null);
        res.moveToFirst();

        while(res.isAfterLast() == false) {
//            Log.d("the id",""+res.getInt(0));
            hp.put(res.getInt(0), res.getLong(1));
            res.moveToNext();
        }

        return hp;
    }

    public HashMap getHistoryModelList() {
        HashMap hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.query(TABLE_NAME, new String[]{COLUMN_ID, COLUMN_START_TIME, COLUMN_READ_TIME, COLUMN_STATUS},null,null,null,null,COLUMN_START_TIME);
        res.moveToFirst();

        while(res.isAfterLast() == false) {
//            Log.d("the id",""+res.getInt(0));
            hp.put(res.getInt(0), new HistoryModel(res.getLong(1), res.getLong(2), res.getInt(3)));
            res.moveToNext();
        }
        res.close();
        return hp;
    }
}