package helloworld.environmenttag;

import android.content.Context;
import android.util.Log;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by king on 7/11/2015.
 */
public class DataStore implements Serializable {
    private long[] timestamps;
    private long start_time;
    private long insertTime;
    private String serialNumebr;
    private float upperLimit;
    private float lowerLimit;
    private int time_interval, num_of_record;
    private ConfigObject mConfigObject;
    private List<Float> tempera = new ArrayList<Float>();

    // Create new DataStore
    public DataStore(String serialNum,long startTime, int interval,float[] temp, float upperLi, float lowerLi, int numOfRecord) {
        serialNumebr = serialNum;
        start_time = startTime;
        time_interval = interval;
        upperLimit = upperLi;
        lowerLimit = lowerLi;
        tempera = float_to_Float(temp);
        num_of_record = numOfRecord;
        this.setTimes(start_time, interval, tempera.size() + 1);
    }
    // For db to DataStore
    public DataStore(String serialNum, int interval, float upperLi, float lowerLi,
                     long startTime, String temp, long insertTi, int numOfRecord) {
        time_interval = interval;
        serialNumebr = serialNum;
        upperLimit = upperLi;
        lowerLimit = lowerLi;
        start_time = startTime;
        tempera = StringtoFloatArray(temp);
        insertTime = insertTi;
        num_of_record = numOfRecord;
        this.setTimes(start_time, interval, tempera.size() + 1);
    }

    public List<Float> float_to_Float(float[] arr){
        List<Float> result = new ArrayList<Float>(arr.length);
        for (float f : arr) {
            result.add(Float.valueOf(f));
        }
        return result;

    }

    public ArrayList getTimeString_in_arraylist(String dateformat){
        ArrayList<String> output = new ArrayList<String>();
        Log.d("size of time",""+timestamps.length);
        for(int k = 0; k < timestamps.length; k++){
            Date temp = new Date(start_time + k * time_interval * 1000);
            SimpleDateFormat df2 = new SimpleDateFormat(dateformat);
            String dateText = df2.format(temp);
            output.add(dateText);
        }
        return output;
    }

    private long Byte_to_date(byte[] date){
        Date temp;
        temp  = new Date(100+date[0],date[1],date[2],date[3],date[4],date[5]);
        return temp.getTime();
    }

    public void setTimes(byte[] starting_time,byte time_int,byte interval_unit,byte[] num_of_count){
        int unit;
        start_time = Byte_to_date(starting_time);
        num_of_record = (num_of_count[0]<<8) + num_of_count[1];
        switch(interval_unit){
            case 0:
                unit = 1000;//1 sec
                break;
            case 1:
                unit = 60*1000;//1 min
                break;
            case 2:
                unit = 60*60*1000;//1 hr
                break;
            default:
                unit = 1000;//1 sec
                break;
        }
        time_interval = time_int * unit;
//        timestamps = new long[num_of_record];
//        for(i=0;i<num_of_record;i++){
//            timestamps[i] =  start_time+i*time_interval;
//        }
        setTimes(start_time,time_interval,num_of_record);
    }

    public void setTimes(long start_time,int interval,int num_of_count){
        int i;
        timestamps = new long[num_of_count];
        Log.d("timestamp size",""+ num_of_count+" "+timestamps.length);

        for(i=0;i<num_of_record;i++){
            timestamps[i] =  start_time+i*interval;
            Log.d("timestamp",""+timestamps[i]);
        }
    }


//    private float raw_to_temperature(short raw){
//        int  temp;
//        temp = (raw /10) -40;
//        return (float)temp;
//    }

//    public void append_temperature(byte[] raw){
//        short i;
//        byte data_unit = 2;
//
//        for(i=0;i<raw.length-1;i+=data_unit){
//            if(raw[i] == (byte)0xFF)
//                break;
//            tempera.add(raw_to_temperature((short)((raw[i]<<8)+raw[i+1])));
//        }
//    }

    public void setInsertTime(long time) {
        insertTime = time;
    }

    public long getInsertTime() {
        return insertTime;
    }

    public List<Float> getTemperatures() {
        return tempera;
    }

    // TODO
    public long writeToDB(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        return dbHelper.insertHistory(start_time, time_interval, FloatArraytoString(tempera),
                serialNumebr, insertTime, upperLimit, lowerLimit, num_of_record);
    }

    // Split an string with , to a array
    // TODO: Move to a util class
    private List<Float> StringtoFloatArray(String string) {
        List<Float> array = new ArrayList<Float>();
        for (String retval : string.split(",")) {
            array.add(Float.parseFloat(retval));
        }
        return array;
    }
    private String FloatArraytoString(List<Float> array) {
        String string = "";
        for (Float num : array) {
            string += num.toString() + ",";
        }
        string.substring(0, string.length() - 2);
        return string;
    }

    public ConfigObject getConfigObject() {
        if (mConfigObject == null) {
            mConfigObject = new ConfigObject(start_time, insertTime, serialNumebr, upperLimit, lowerLimit, time_interval, num_of_record);
        }
        return mConfigObject;
    }

    public class ConfigObject {
        public String serialNumebr;
        public long startTime, insertTime;
        public float upperLimit, lowerLimit;
        public int time_interval, num_of_record;
        public ConfigObject(long _startTime, long _insertTime, String _serialNumebr, float _upperLimit, float _lowerLimit, int _timeInterval, int _numOfRecord) {
            startTime = _startTime;
            insertTime = _insertTime;
            serialNumebr = _serialNumebr;
            upperLimit = _upperLimit;
            lowerLimit = _lowerLimit;
            time_interval = _timeInterval;
            num_of_record = _numOfRecord;
        }
    }

}
