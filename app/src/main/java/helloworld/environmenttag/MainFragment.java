package helloworld.environmenttag;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import helloworld.environmenttag.HIDHelper.HIDEventListener;

/**
 * Created by king on 5/6/2016.
 */
public class MainFragment extends Fragment implements HIDEventListener{
    private EnvoTag_HID tag;
    private My_LineChart mChart;
//    private TextView t;
    private List<Float> temperature;
    private long currentRowID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.content_main, container, false);
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Update Action Bar
        ActionBar actionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        actionBar.setTitle("EnvoTag");

        tag = EnvoTag_HID.getInstance(getContext());
        tag.clearEventListener();
        tag.addEventListener("attach",this);
        tag.addEventListener("detach",this);

        mChart = new My_LineChart(getView().findViewById(R.id.chart));

//        t = new TextView(getContext());
//        t = (TextView) getView().findViewById(R.id.textView);

        // Detect EnvoTag
        if(tag.isRunning()) {
//            t.setText("EnvoTag detected");
            hideModal();
        }
        else {
            showModal();
//            t.setText("Please connect EnvoTag");
        }

        // For debug usage
/*        float[] temp = {1,2,3,4,5,6,7};
        Date date = new Date();
        for (int i=0;i<10;i++) {
            DataStore _data = new DataStore("" + i,date.getTime(),60,temp,13f,19f);
            _data.setInsertTime(date.getTime());
            _data.writeToDB(getContext());
        }*/
    }

    public void onSelectAdd(View view) {
        float[] tempArray = {};
        int counter = 0;
        int numOfRecord = (int)tag.getMiscInfo().get("RECORD_NUM");

        // re-try max 3 times reading all temperature record if no. of record doesn't match
        do{
            tempArray = tag.readAllTemp();
            counter++;
        } while(!tag.checkNumOfRec(tempArray, numOfRecord) && counter < 3);

        if (tempArray.length > 0) {
            Date date = new Date();
            DataStore _data = new DataStore((String)tag.getMiscInfo().get("SERIAL_NUM"), (long)tag.getMiscInfo().get("START_TIME"),
                    (int)tag.getMiscInfo().get("INTERVAL"), tempArray, (float)tag.getMiscInfo().get("HI_TEMP"),
                    (float)tag.getMiscInfo().get("LO_TEMP"), (int)tag.getMiscInfo().get("RECORD_NUM"));
            _data.setInsertTime(date.getTime());
            currentRowID = _data.writeToDB(getContext());

            ArrayList<String> xVals = _data.getTimeString_in_arraylist("HH:mm:ss");
            temperature=_data.getTemperatures();
            mChart.setData(xVals, temperature);
//            t.setText("");
//            for (int i = 0; i < tempArray.length; i++){
//                t.append(tempArray[i] + "°C ");
//            }
        } else {
            // For Debug purpose
//            t.setText("No record read");
        }
    }

    public void onSelectNoOfRec(View view) {
//        int numOfRec = tag.getNumOfRec();
//
//        t.setText("No. of record = " + numOfRec);

        HashMap hmap = tag.getMiscInfo();

//        t.setText("Start time = " + hmap.get("START_TIME") + "\n");
//        t.append("No. of record = " + hmap.get("RECORD_NUM") + "\n");
//        t.append("Out range = " + hmap.get("OUT_RANGE_NUM") + "\n");
//        t.append("Interval = " + hmap.get("INTERVAL") + "\n");
//        t.append("Serial No. = " + hmap.get("SERIAL_NUM") + "\n");
//        t.append("Hi Temp = " + hmap.get("HI_TEMP") + "\n");
//        t.append("Lo Temp = " + hmap.get("LO_TEMP") + "\n");
    }

    public void onSelectReadCurrentTemp(View view) {
        float currentTemp = tag.readCurrentTemp();

//        t.setText("Current Temperature = " + currentTemp + "°C");
    }

    public void onSelectChart(View view) {
        if (currentRowID != -1) {
            DetailsFragment newFragment = new DetailsFragment();
            Bundle args = new Bundle();
            //HistoryListChild currentChild= (HistoryListChild) listDataChild.get(listHeader.get(groupPosition));
            args.putLong("ID", currentRowID);
            newFragment.setArguments(args);

            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commit();
        }
    }


    public void showModal() {
        getView().findViewById(R.id.modal).setVisibility(View.VISIBLE);
    }

    public void hideModal() {
        getView().findViewById(R.id.modal).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDeviceFound(){
        Toast.makeText(getContext(),"found",Toast.LENGTH_SHORT);
//        TextView t =(TextView) getView().findViewById(R.id.textView);
//        t.setText("EnvoTag detected");

        hideModal();
    }

    @Override
    public void onDeviceDetached(){
//        TextView t =(TextView) getView().findViewById(R.id.textView);
//        t.setText("Please connect EnvoTag");

        showModal();
    }

    @Override
    public void onDestroyView(){
        tag.clearEventListener();
        super.onDestroyView();

    }
}
